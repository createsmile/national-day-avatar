#### 功能
快速制作国庆头绪

#### 展示

[https://createsmile.gitee.io/national-day-avatar](https://createsmile.gitee.io/national-day-avatar)

#### 图片

![国庆头像生成展示](https://images.gitee.com/uploads/images/2021/1001/162543_8eabf58f_8636491.jpeg "微信图片_20211001162515.jpg")

#### 说明

代码非原创，gitee一搜有好多个，不知原作是谁，感谢分享 :heart: 